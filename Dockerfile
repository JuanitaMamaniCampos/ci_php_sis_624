FROM debian:latest
RUN apt update -y
RUN apt install apache2 libapache2-mod-php php php-mysql -y
COPY public /var/www/html/
ENV MYSQL_HOST=localhost
ENV MYSQL_USERNAME=root
ENV MYSQL_PASSWORD=password
ENV MYSQL_DB=juanitamamanidb
EXPOSE 80

CMD ["/bin/bash","-c","set -e; apachectl -D FOREGROUND \"$@\""]
