<?php include 'header.php';?>
<section class="vh-100">
  <div class="container py-5 h-100">
    <div class="row d-flex align-items-center justify-content-center h-100">
      <div class="col-md-8 col-lg-7 col-xl-6">
        <img src="https://mdbootstrap.com/img/Photos/new-templates/bootstrap-login-form/draw2.svg" class="img-fluid" alt="Phone image>      </div>
      <div class="col-md-7 col-lg-5 col-xl-5 offset-xl-1">
        <h1>REGISTRO</h1>
         <form action="registrar.php" method="post">
          <div class="form-outline mb-4">
            <input type="int" name="ci" class="form-control form-control-lg" //>
            <label class="form-label">CI</label>
          </div>

          <div class="form-outline mb-4">
            <input type="text" name="nombre" class="form-control form-control-lg" />
            <label class="form-label">NOMBRE</label>
          </div>

          <div class="form-outline mb-4">
            <input type="email" name="email" id="form1Example13" class="form-control form-control-lg" />
            <label class="form-label">EMAIL</label>
          </div>

          <div class="form-outline mb-4">
            <input type="int" name="telefono" id="form1Example23" class="form-control form-control-lg" />
            <label class="form-label">TELEFONO</label>
          </div>
          <!-- Submit button -->
          <button type="submit" class="btn btn-primary btn-lg btn-block">REGISTRAR</button>

          <div class="divider d-flex align-items-center my-4">
            <p class="text-center fw-bold mx-3 mb-0 text-muted">OR</p>
          </div>

          <a class="btn btn-primary btn-lg btn-block" target="_blank" style="background-color: #3b5998" href="https://es-la.facebook.>            <i class="fab fa-facebook-f me-2"></i>Continue with Facebook
          </a>
          <a class="btn btn-primary btn-lg btn-block" target="_blank" style="background-color: #55acee" href="https://mobile.twitter.>            <i class="fab fa-twitter me-2"></i>Continue with Twitter</a>

        </form>
      </div>
    </div>
  </div>
</section>


<?php include 'fooater.php';?>
