<?php
    include("conexion.php");
    $usuarios ="SELECT * FROM estudiante";
?>
<?php include 'header.php';?>
<table class="table table-striped">
        <thead>
        <tr>
            <th>CI</th>
            <th>NOMBRE</th>
            <th>EMAIL</th>
            <th>TELEFONO</th>
            <th>EDITAR</th>
            <th>ELIMINAR</th>
        </tr>
        </thead>
<tr>
        <?php $resultado= mysqli_query($connection, $usuarios);
        while($row=mysqli_fetch_assoc($resultado)) {?>
            <th><?php echo $row["ci"];?></th>
            <th><?php echo $row["nombre"];?></th>
            <th><?php echo $row["email"];?></th>
            <th><?php echo $row["telefono"];?></th>
            <th class="table-item">
                <a id="nuevo" href="actualizar.php?ci=<?php echo $row["ci"];?>" class="table--item--link"><i class="fas fa-edit"></i></a>
            </th>
            <th>
                <a id="nuevo" href="procesar_eliminar.php?ci=<?php echo $row["ci"];?>" class="esto_se_elimina"><i class="fas fa-erase"></i></a>
            </th>
</tr>
            <?php } mysqli_free_result($resultado); ?>
</table>
<?php include 'fooater.php';?>
